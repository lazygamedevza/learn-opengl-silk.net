﻿using System.Numerics;
using LearnOpenGL.Utilities;
using Silk.NET.Input;
using Silk.NET.Maths;
using Silk.NET.OpenGL;
using Silk.NET.Windowing;
using StbiSharp;
using Shader = LearnOpenGL.Utilities.Shader;

namespace LearnOpenGL.Lighting;

public class LightingApp : IDisposable
{
    private static readonly Vector2D<int> WindowSize = new(800, 600);
    private static readonly float[] Vertices = [
        // positions          // normals           // texture coords
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
    ];

    private static readonly Vector3[] cubePositions = [
        new Vector3( 0.0f,  0.0f,  0.0f),
        new Vector3( 2.0f,  5.0f, -15.0f),
        new Vector3(-1.5f, -2.2f, -2.5f),
        new Vector3(-3.8f, -2.0f, -12.3f),
        new Vector3( 2.4f, -0.4f, -3.5f),
        new Vector3(-1.7f,  3.0f, -7.5f),
        new Vector3( 1.3f, -2.0f, -2.5f),
        new Vector3( 1.5f,  2.0f, -2.5f),
        new Vector3( 1.5f,  0.2f, -1.5f),
        new Vector3(-1.3f,  1.0f, -1.5f)
    ];

    private static readonly Vector3[] pointLightPositions = [
	    new Vector3( 0.7f,  0.2f,  2.0f),
	    new Vector3( 2.3f, -3.3f, -4.0f),
	    new Vector3(-4.0f,  2.0f, -12.0f),
	    new Vector3( 0.0f,  0.0f, -3.0f)
    ];

    private readonly IWindow window;
    private IKeyboard primaryKeyboard;
    private GL GL;
    private uint VBO;
    private uint cubeVAO;
    private uint lightCubeVAO;
    private uint diffuseTexture;
    private uint specularTexture;
    private Shader lightingShader;
    private Shader lightingCubeShader;

    private Camera camera = new(new Vector3(0.0f, 0.0f, 3.0f), Vector3.UnitY);
    private Vector2D<float> lastMousePosisition = WindowSize.As<float>() / 2.0f;

    private Vector3 lightPos = new(1.2f, 1.0f, 2.0f);

    public LightingApp()
    {
        var options = WindowOptions.Default;
        options.Size = WindowSize;
        options.Title = "Learn OpenGL with Silk.NET";

        window = Window.Create(options);
        window.Load += WindowOnLoad;
        window.Render += WindowOnRender;
        window.Update += WindowOnUpdate;
        window.FramebufferResize += WindowOnFramebufferResize;
        window.Closing += WindowOnClosing;
    }

    public int Run()
    {
        window.Run();
        
        return 0;
    }

    public void Dispose()
    {
        GL.Dispose();
        window.Dispose();
    }

    private void WindowOnLoad()
    {
        var input = window.CreateInput();

        primaryKeyboard = input.Keyboards.First();
        primaryKeyboard.KeyDown += KeyboardOnKeyDown;

        foreach (var mouse in input.Mice)
        {
            mouse.Cursor.CursorMode = CursorMode.Raw;
            mouse.MouseMove += InitialMouseOnMouseMove;
            mouse.Scroll += MouseOnScroll;
        }
        
        GL = GL.GetApi(window);
        
        GL.Enable(EnableCap.DepthTest);

        lightingShader = new Shader(GL, "Assets/vert.colours.glsl", "Assets/frag.colours.glsl");
        lightingCubeShader = new Shader(GL, "Assets/vert.lighting.glsl", "Assets/frag.lighting.glsl");

        cubeVAO = GL.GenVertexArray();
        VBO = GL.GenBuffer();

        GL.BindBuffer(BufferTargetARB.ArrayBuffer, VBO);
        GL.BufferData<float>(BufferTargetARB.ArrayBuffer, Vertices, BufferUsageARB.StaticDraw);

        GL.BindVertexArray(cubeVAO);
        
        // position attribute
        const int strideCount = 8;
        GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, strideCount * sizeof(float), 0);
        GL.EnableVertexAttribArray(0);
        // normal attribute
        GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, strideCount * sizeof(float), 3 * sizeof(float));
        GL.EnableVertexAttribArray(1);
        // texture coords attribute
        GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, strideCount * sizeof(float), 6 * sizeof(float));
        GL.EnableVertexAttribArray(2);

        lightCubeVAO = GL.GenVertexArray();
        GL.BindVertexArray(lightCubeVAO);
        
        GL.BindBuffer(BufferTargetARB.ArrayBuffer, VBO);
        
        // position attribute
        GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, strideCount * sizeof(float), 0);
        GL.EnableVertexAttribArray(0);

        diffuseTexture = GL.GenTexture();
        GL.BindTexture(TextureTarget.Texture2D, diffuseTexture);
        // set the texture wrapping/filtering options (on the currently bound texture object)
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

        using var diffuseTextureStream = File.OpenRead("Assets/container2.png");
        using var diffuseTextureMemoryStream = new MemoryStream();
        diffuseTextureStream.CopyTo(diffuseTextureMemoryStream);
        using var diffuseTextureImage = Stbi.LoadFromMemory(diffuseTextureMemoryStream, 3);
        
        GL.TexImage2D(TextureTarget.Texture2D, 0 , InternalFormat.Rgb, (uint)diffuseTextureImage.Width, (uint)diffuseTextureImage.Height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, diffuseTextureImage.Data);
        GL.GenerateMipmap(TextureTarget.Texture2D);

        specularTexture = GL.GenTexture();
        GL.BindTexture(TextureTarget.Texture2D, specularTexture);
        // set the texture wrapping/filtering options (on the currently bound texture object)
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

        using var specularTextureStream = File.OpenRead("Assets/container2_specular.png");
        using var specularTextureMemoryStream = new MemoryStream();
        specularTextureStream.CopyTo(specularTextureMemoryStream);
        using var specularTextureImage = Stbi.LoadFromMemory(specularTextureMemoryStream, 3);
        
        GL.TexImage2D(TextureTarget.Texture2D, 0 , InternalFormat.Rgb, (uint)specularTextureImage.Width, (uint)specularTextureImage.Height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, specularTextureImage.Data);
        GL.GenerateMipmap(TextureTarget.Texture2D);
        
        lightingShader.Use();
        lightingShader.SetInt("material.diffuse", 0);
        lightingShader.SetInt("material.specular", 1);
    }

    private void WindowOnRender(double deltaTime)
    {
        GL.ClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

        GL.ActiveTexture(TextureUnit.Texture0);
        GL.BindTexture(TextureTarget.Texture2D, diffuseTexture);
        GL.ActiveTexture(TextureUnit.Texture1);
        GL.BindTexture(TextureTarget.Texture2D, specularTexture);
        
        lightingShader.Use();
        lightingShader.SetInt("material.diffuse", 0);
        lightingShader.SetVector3("material.specular", 0.5f, 0.5f, 0.5f);
        lightingShader.SetFloat("material.shininess", 32.0f);

        var lightColour = Vector3.One;

        var diffuseColour = lightColour * 0.5f; 
        var ambientColour = lightColour * 0.2f;
        
        lightingShader.SetVector3("dirLight.direction", -0.2f, -1.0f, -0.3f);
        lightingShader.SetVector3("dirLight.ambient", ambientColour);
        lightingShader.SetVector3("dirLight.diffuse", diffuseColour);
        lightingShader.SetVector3("dirLight.specular", lightColour);

        for (int i = 0; i < pointLightPositions.Length; i++)
        {
            lightingShader.SetVector3($"pointLights[{i}].position", pointLightPositions[i]);
            lightingShader.SetVector3($"pointLights[{i}].ambient", ambientColour);
            lightingShader.SetVector3($"pointLights[{i}].diffuse", diffuseColour);
            lightingShader.SetVector3($"pointLights[{i}].specular", lightColour);
            lightingShader.SetFloat($"pointLights[{i}].constant", 1.0f);
            lightingShader.SetFloat($"pointLights[{i}].linear", 0.09f);
            lightingShader.SetFloat($"pointLights[{i}].quadratic", 0.032f);
        }

        lightingShader.SetVector3("spotLight.position", camera.Position);
        lightingShader.SetVector3("spotLight.direction", camera.Front);
        lightingShader.SetFloat("spotLight.cutOff", Scalar.Cos(Scalar.DegreesToRadians(12.5f)));
        lightingShader.SetFloat("spotLight.outerCutOff", Scalar.Cos(Scalar.DegreesToRadians(17.5f)));
        lightingShader.SetVector3("spotLight.ambient", ambientColour);
        lightingShader.SetVector3("spotLight.diffuse", diffuseColour);
        lightingShader.SetVector3("spotLight.specular", lightColour);
        lightingShader.SetFloat("spotLight.constant", 1.0f);
        lightingShader.SetFloat("spotLight.linear", 0.09f); 
        lightingShader.SetFloat("spotLight.quadratic", 0.032f);

        lightingShader.SetVector3("viewPos", camera.Position);

        var projection = Matrix4x4.CreatePerspectiveFieldOfView(float.DegreesToRadians(camera.FieldOfView),
            (float)WindowSize.X / WindowSize.Y, 0.1f, 100.0f);
        var view = camera.ViewMatrix;
        lightingShader.SetMatrix4(nameof(projection), in projection);
        lightingShader.SetMatrix4(nameof(view), in view);

        Matrix4x4 model;
   
        for (var i = 0; i < cubePositions.Length; i++)
        {
            model = Matrix4x4.Identity;
            var angle = 20.0f * i;
            model *= Matrix4x4.CreateFromAxisAngle(Vector3.Normalize(new Vector3(1.0f, 0.3f, 0.5f)), angle);
            model *= Matrix4x4.CreateTranslation(cubePositions[i]);
            lightingShader.SetMatrix4(nameof(model), in model);
        
            GL.BindVertexArray(cubeVAO);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 36);
        }
        
        lightingCubeShader.Use();
        lightingCubeShader.SetMatrix4(nameof(projection), in projection);
        lightingCubeShader.SetMatrix4(nameof(view), in view);

        for (int i = 0; i < pointLightPositions.Length; i++)
        {
            model = Matrix4x4.Identity;
            model *= Matrix4x4.CreateScale(new Vector3(0.2f));
            model *= Matrix4x4.CreateTranslation(pointLightPositions[i]);
            lightingCubeShader.SetMatrix4(nameof(model), in model);
        
            GL.BindVertexArray(lightCubeVAO);
            GL.DrawArrays(PrimitiveType.Triangles, 0, 36);
        }
    }

    private void WindowOnUpdate(double deltaTime)
    {
        if (primaryKeyboard.IsKeyPressed(Key.W))
        {
            camera.ProcessLateralMovement(Camera.Movement.Forward, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.S))
        {
            camera.ProcessLateralMovement(Camera.Movement.Backward, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.A))
        {
            camera.ProcessLateralMovement(Camera.Movement.Left, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.D))
        {
            camera.ProcessLateralMovement(Camera.Movement.Right, (float)deltaTime);
        }
    }

    private void KeyboardOnKeyDown(IKeyboard keyboard, Key keyDown, int _)
    {
        if (keyDown == Key.Escape)
        {
            window.Close();
        }
    }

    private void InitialMouseOnMouseMove(IMouse mouse, Vector2 position)
    {
        lastMousePosisition = position.ToGeneric();

        mouse.MouseMove -= InitialMouseOnMouseMove;
        mouse.MouseMove += MouseOnMouseMove;
    }

    private void MouseOnMouseMove(IMouse mouse, Vector2 position)
    {
        var mousePosition = position.ToGeneric();
        var offset = mousePosition - lastMousePosisition;
        lastMousePosisition = mousePosition;

        camera.ProcessLookMovement(Vector2D.Reflect(offset, Vector2D<float>.UnitY));
    }

    private void MouseOnScroll(IMouse mouse, ScrollWheel scrollWheel)
    {
        camera.ProcessZoom(scrollWheel.Y);
    }

    private void WindowOnFramebufferResize(Vector2D<int> newSize)
    {
        GL.Viewport(newSize);
    }

    private void WindowOnClosing()
    {
        GL.DeleteBuffer(VBO);
        GL.DeleteVertexArray(cubeVAO);
        GL.DeleteVertexArray(lightCubeVAO);
        
        lightingShader.Dispose();
        lightingCubeShader.Dispose();
    }
}