﻿using System.Numerics;
using LearnOpenGL.Utilities;
using Silk.NET.Input;
using Silk.NET.Maths;
using Silk.NET.OpenGL;
using Silk.NET.Windowing;
using StbiSharp;
using Shader = LearnOpenGL.Utilities.Shader;

namespace LearnOpenGL.GettingStarted;

public class GettingStartedApp : IDisposable
{
    private static readonly Vector2D<int> WindowSize = new(800, 600);

    private static readonly float[] Verticies =
    [
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
        0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
        0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
        0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
        0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
        0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    ];

    private static Vector3[] cubePositions = 
    [
        new Vector3( 0.0f,  0.0f,  0.0f), 
        new Vector3( 2.0f,  5.0f, -15.0f), 
        new Vector3(-1.5f, -2.2f, -2.5f),  
        new Vector3(-3.8f, -2.0f, -12.3f),  
        new Vector3( 2.4f, -0.4f, -3.5f),  
        new Vector3(-1.7f,  3.0f, -7.5f),  
        new Vector3( 1.3f, -2.0f, -2.5f),  
        new Vector3( 1.5f,  2.0f, -2.5f), 
        new Vector3( 1.5f,  0.2f, -1.5f), 
        new Vector3(-1.3f,  1.0f, -1.5f)  
    ];
    
    private readonly IWindow window;
    private IKeyboard primaryKeyboard;
    private GL GL;
    private uint VBO;
    private uint VAO;
    private uint EBO;
    private uint texture1, texture2;
    private Shader shader;

    private Camera camera = new( Vector3.UnitZ * 3.0f, Vector3.UnitY);
    private Vector2D<float> lastMousePosisition = (Vector2D<float>)WindowSize / 2.0f;

    public GettingStartedApp()
    {
        var options = WindowOptions.Default;
        options.Size = WindowSize;
        options.Title = "Learn OpenGL with Silk.NET";

        window = Window.Create(options);
        window.Load += WindowOnLoad;
        window.Render += WindowOnRender;
        window.Update += WindowOnUpdate;
        window.FramebufferResize += WindowOnFramebufferResize;
        window.Closing += WindowOnClosing;
    }

    public int Run()
    {
        window.Run();

        return 0;
    }

    public void Dispose()
    {
        GL.Dispose();
        window.Dispose();
    }

    private void WindowOnLoad()
    {
        var input = window.CreateInput();

        primaryKeyboard = input.Keyboards.First();
        primaryKeyboard.KeyDown += KeyboardOnKeyDown;

        foreach (var mouse in input.Mice)
        {
            mouse.Cursor.CursorMode = CursorMode.Raw;
            mouse.MouseMove += InitialMouseOnMouseMove;
            mouse.Scroll += MouseOnScroll;
        }
        
        GL = GL.GetApi(window);

        shader = new Shader(GL, new FileInfo("Assets/vert.glsl"), new FileInfo("Assets/frag.glsl"));

        GL.GenVertexArrays(1, out VAO);
        GL.GenBuffers(1, out VBO);
        GL.GenBuffers(1, out EBO);

        GL.BindVertexArray(VAO);

        GL.BindBuffer(BufferTargetARB.ArrayBuffer, VBO);
        GL.BufferData<float>(BufferTargetARB.ArrayBuffer, Verticies, BufferUsageARB.StaticDraw);

        // position attribute
        GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
        GL.EnableVertexAttribArray(0);
        
        // texture attribute
        GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));
        GL.EnableVertexAttribArray(1);

        GL.GenTextures(1, out texture1);
        GL.BindTexture(TextureTarget.Texture2D, texture1);
        // set the texture wrapping/filtering options (on the currently bound texture object)
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
        // load and generate the texture
        using var texture1Stream = File.OpenRead("Assets/container.jpg");
        using var texture1MemoryStream = new MemoryStream();
        texture1Stream.CopyTo(texture1MemoryStream);
        using var texture1Image = Stbi.LoadFromMemory(texture1MemoryStream, 3);
        
        GL.TexImage2D(TextureTarget.Texture2D, 0, InternalFormat.Rgb, (uint)texture1Image.Width, (uint)texture1Image.Height, 0, PixelFormat.Rgb, PixelType.UnsignedByte, texture1Image.Data);
        GL.GenerateMipmap(TextureTarget.Texture2D);

        GL.GenTextures(1, out texture2);
        GL.BindTexture(TextureTarget.Texture2D, texture2);
        
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
        GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

        using var texture2Stream = File.OpenRead("Assets/awesomeface.png");
        using var texture2MemoryStream = new MemoryStream();
        texture2Stream.CopyTo(texture2MemoryStream);
        Stbi.SetFlipVerticallyOnLoad(true);
        using var texture2Image = Stbi.LoadFromMemory(texture2MemoryStream, 4);
        
        GL.TexImage2D(TextureTarget.Texture2D, 0, InternalFormat.Rgba, (uint)texture2Image.Width, (uint)texture2Image.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, texture2Image.Data);
        GL.GenerateMipmap(TextureTarget.Texture2D);
        
        shader.Use();
        shader.SetInt("texture1", 0);
        shader.SetInt("texture2", 1);
    }

    private void WindowOnRender(double deltaTime)
    {
        GL.Enable(EnableCap.DepthTest);
        GL.ClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        
        // bind Texture
        GL.ActiveTexture(TextureUnit.Texture0);
        GL.BindTexture(TextureTarget.Texture2D, texture1);
        GL.ActiveTexture(TextureUnit.Texture1);
        GL.BindTexture(TextureTarget.Texture2D, texture2);

        // render container
        GL.BindVertexArray(VAO);

        var projection =
            Matrix4x4.CreatePerspectiveFieldOfView(Scalar.DegreesToRadians(camera.FieldOfView), ((float)WindowSize.X) / WindowSize.Y, 0.1f, 100.0f);
        shader.SetMatrix4(nameof(projection), in projection);

        var view = camera.ViewMatrix;
        shader.SetMatrix4(nameof(view), in view);

        for (var index = 0; index < cubePositions.Length; index++)
        {
            var position = cubePositions[index];
            // meths
            var model = Matrix4x4.Identity;
            var angle = 20.0f * index;
            model = model * Matrix4x4.CreateFromAxisAngle(Vector3.Normalize(new Vector3(1.0f, 0.3f, 0.5f)),
                float.DegreesToRadians(angle));
            model = model * Matrix4x4.CreateTranslation(position);

            shader.SetMatrix4(nameof(model), in model);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 36);
            // GL.DrawElements(PrimitiveType.Triangles, (uint)Indices.Length, DrawElementsType.UnsignedInt, null);
        }
    }

    private void WindowOnUpdate(double deltaTime)
    {
        if (primaryKeyboard.IsKeyPressed(Key.W))
        {
            camera.ProcessLateralMovement(Camera.Movement.Forward, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.S))
        {
            camera.ProcessLateralMovement(Camera.Movement.Backward, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.A))
        {
            camera.ProcessLateralMovement(Camera.Movement.Left, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.D))
        {
            camera.ProcessLateralMovement(Camera.Movement.Right, (float)deltaTime);
        }
    }

    private void KeyboardOnKeyDown(IKeyboard keyboard, Key keyDown, int _)
    {
        if (keyDown == Key.Escape)
        {
            window.Close();
        }
    }

    private void InitialMouseOnMouseMove(IMouse mouse, Vector2 position)
    {
        lastMousePosisition = position.ToGeneric();

        mouse.MouseMove -= InitialMouseOnMouseMove;
        mouse.MouseMove += MouseOnMouseMove;
    }

    private void MouseOnMouseMove(IMouse mouse, Vector2 position)
    {
        var mousePosition = position.ToGeneric();
        var offset = mousePosition - lastMousePosisition;
        lastMousePosisition = mousePosition;

        camera.ProcessLookMovement(Vector2D.Reflect(offset, Vector2D<float>.UnitY));
    }

    private void MouseOnScroll(IMouse mouse, ScrollWheel scrollWheel)
    {
        camera.ProcessZoom(scrollWheel.Y);
    }

    private void WindowOnClosing()
    {
        GL.DeleteBuffer(VBO);
        GL.DeleteBuffer(EBO);
        GL.DeleteVertexArray(VAO);
        
        shader.Dispose();
    }

    private void WindowOnFramebufferResize(Vector2D<int> newSize)
    {
        GL.Viewport(newSize);
    }
}