﻿using System.Numerics;
using LearnOpenGL.Utilities;
using Silk.NET.Input;
using Silk.NET.Maths;
using Silk.NET.OpenGL;
using Silk.NET.Windowing;
using StbiSharp;
using Shader = LearnOpenGL.Utilities.Shader;

namespace LearnOpenGL.ModelLoading;

public class ModelLoadingApp : IDisposable
{
    // settings
    private static readonly Vector2D<int> WindowSize = new(800, 600);

    
    // camera
    private Camera camera = new(new Vector3(0.0f, 0.0f, 3.0f), Vector3.UnitY);
    private Vector2D<float> lastMousePosisition = WindowSize.As<float>() / 2.0f;
    
    private readonly IWindow window;
    private IKeyboard primaryKeyboard;
    private GL GL;
    private Shader modelLoadingShader;
    private Model model;

    public ModelLoadingApp()
    {
        var options = WindowOptions.Default;
        options.Size = WindowSize;
        options.Title = "Learn OpenGL with Silk.NET";

        window = Window.Create(options);
        window.Load += WindowOnLoad;
        window.Render += WindowOnRender;
        window.Update += WindowOnUpdate;
        window.FramebufferResize += WindowOnFramebufferResize;
        window.Closing += WindowOnClosing;
    }
    public int Run()
    {
        window.Run();
        
        return 0;
    }

    public void Dispose()
    {
        GL.Dispose();
        
    }

    private void WindowOnLoad()
    {
        var input = window.CreateInput();

        primaryKeyboard = input.Keyboards.First();
        primaryKeyboard.KeyDown += KeyboardOnKeyDown;

        foreach (var mouse in input.Mice)
        {
            mouse.Cursor.CursorMode = CursorMode.Raw;
            mouse.MouseMove += InitialMouseOnMouseMove;
            mouse.Scroll += MouseOnScroll;
        }
        
        GL = window.CreateOpenGL();

        Stbi.SetFlipVerticallyOnLoad(true);
        
        GL.Enable(EnableCap.DepthTest);

        modelLoadingShader = new Shader(GL, "Assets/model_loading.vert", "Assets/model_loading.frag");

        model = new Model(GL, new DirectoryInfo("Assets/Objects/Backpack/backpack.obj"));
    }

    private void WindowOnRender(double deltaTime)
    {
        GL.ClearColor(0.05f, 0.05f, 0.05f, 1.0f);
        GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

        modelLoadingShader.Use();

        var projection = Matrix4x4.CreatePerspectiveFieldOfView(float.DegreesToRadians(camera.FieldOfView),
            (float)WindowSize.X / WindowSize.Y, 0.1f, 100.0f);
        var view = camera.ViewMatrix;
        modelLoadingShader.SetMatrix4(nameof(projection), projection);
        modelLoadingShader.SetMatrix4(nameof(view), view);

        Matrix4x4 model = Matrix4x4.Identity;
        model *= Matrix4x4.CreateScale(Vector3.One);
        model *= Matrix4x4.CreateTranslation(Vector3.Zero);
        modelLoadingShader.SetMatrix4(nameof(model), model);

        this.model.Draw(modelLoadingShader);
    }

    private void WindowOnUpdate(double deltaTime)
    {
        if (primaryKeyboard.IsKeyPressed(Key.W))
        {
            camera.ProcessLateralMovement(Camera.Movement.Forward, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.S))
        {
            camera.ProcessLateralMovement(Camera.Movement.Backward, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.A))
        {
            camera.ProcessLateralMovement(Camera.Movement.Left, (float)deltaTime);
        }

        if (primaryKeyboard.IsKeyPressed(Key.D))
        {
            camera.ProcessLateralMovement(Camera.Movement.Right, (float)deltaTime);
        }
    }

    private void KeyboardOnKeyDown(IKeyboard keyboard, Key keyDown, int _)
    {
        if (keyDown == Key.Escape)
        {
            window.Close();
        }
    }

    private void InitialMouseOnMouseMove(IMouse mouse, Vector2 position)
    {
        lastMousePosisition = position.ToGeneric();

        mouse.MouseMove -= InitialMouseOnMouseMove;
        mouse.MouseMove += MouseOnMouseMove;
    }

    private void MouseOnMouseMove(IMouse mouse, Vector2 position)
    {
        var mousePosition = position.ToGeneric();
        var offset = mousePosition - lastMousePosisition;
        lastMousePosisition = mousePosition;

        camera.ProcessLookMovement(Vector2D.Reflect(offset, Vector2D<float>.UnitY));
    }

    private void MouseOnScroll(IMouse mouse, ScrollWheel scrollWheel)
    {
        camera.ProcessZoom(scrollWheel.Y);
    }

    private void WindowOnFramebufferResize(Vector2D<int> newSize)
    {
        GL.Viewport(newSize);
    }

    private void WindowOnClosing()
    {
        modelLoadingShader.Dispose();
    }
}