﻿using System.Numerics;
using Silk.NET.Maths;

namespace LearnOpenGL.Utilities;

public class Camera
{
    public enum Movement
    {
        Forward,
        Backward,
        Left,
        Right
    }


    public static class Constants
    {
        public const float Yaw = -90.0f;
        public const float Pitch = 0.0f;
        public const float Speed = 2.5f;
        public const float LookSensitivity = 0.1f;
        public const float FieldOfViewMin = 1.0f;
        public const float FieldOfViewMax = 45.0f;
    }

    private Vector3 up;
    private Vector3 right;
    private Vector3 worldUp;

    private float yaw;
    private float pitch;

    private float movementSpeed;
    private float lookSensitivity;

    public Camera(Vector3 position, Vector3 worldUp, float yaw = Constants.Yaw, float pitch = Constants.Pitch)
    {
        this.Position = position;
        this.Front = new Vector3(0.0f, 0.0f, -1.0f);
        this.worldUp = worldUp;

        this.yaw = yaw;
        this.pitch = pitch;

        this.movementSpeed = Constants.Speed;
        this.lookSensitivity = Constants.LookSensitivity;
        FieldOfView = Constants.FieldOfViewMax;
        
        UpdateCameraVectors();
    }
    
    public Camera(float posX, float posY, float posZ, float worldUpX, float worldUpY, float worldUpZ, float yaw, float pitch)
        : this(new Vector3(posX, posY, posZ), new Vector3(worldUpX, worldUpY, worldUpZ), yaw, pitch)
    {}

    public Matrix4x4 ViewMatrix => Matrix4x4.CreateLookAt(Position, Position + Front, up);
    public float FieldOfView { get; private set; }

    public Vector3 Position { get; private set; }

    public Vector3 Front { get; private set; }

    public void ProcessLateralMovement(Movement direction, float deltaTime)
    {
        var velocity = movementSpeed * deltaTime;

        if (direction == Movement.Forward)
        {
            Position += Front * velocity;
        }
        if (direction == Movement.Backward)
        {
            Position -= Front * velocity;
        }
        if (direction == Movement.Left)
        {
            Position -= right * velocity;
        }
        if (direction == Movement.Right)
        {
            Position += right * velocity;
        }
    }

    public void ProcessLookMovement(Vector2D<float> offset, bool constrainPitch = true)
    {
        offset *= lookSensitivity;

        yaw += offset.X;
        pitch += offset.Y;

        if (constrainPitch)
        {
            pitch = Math.Clamp(pitch, -89.0f, 89.0f);
        }
        
        UpdateCameraVectors();
    }

    public void ProcessZoom(float offset)
    {
        FieldOfView -= offset;

        FieldOfView = Math.Clamp(FieldOfView, Constants.FieldOfViewMin, Constants.FieldOfViewMax);
    }

    private void UpdateCameraVectors()
    {
        var yawRadians = Scalar.DegreesToRadians(yaw);
        var pitchRadians = Scalar.DegreesToRadians(pitch);
        var front = new Vector3(
            float.Cos(yawRadians) * float.Cos(pitchRadians),
            float.Sin(pitchRadians),
            float.Sin(yawRadians) * float.Cos(pitchRadians)    
        );

        this.Front = Vector3.Normalize(front);
        this.right = Vector3.Normalize(Vector3.Cross(this.Front, worldUp));
        this.up = Vector3.Normalize(Vector3.Cross(right, this.Front));
    }
}