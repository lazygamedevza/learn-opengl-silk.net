﻿using System.Numerics;

namespace LearnOpenGL.Utilities;

public struct Vertex
{
    public Vector3 Position;
    public Vector3 Normal;
    public Vector3 Tangent;
    public Vector2 TexCoords;
    public Vector3 Bitangent;

    public const int MAX_BONE_INFLUENCE = 4;
    public unsafe fixed int BoneIds[MAX_BONE_INFLUENCE];
    public unsafe fixed float Weights[MAX_BONE_INFLUENCE];
}