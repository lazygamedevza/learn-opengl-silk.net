﻿using Silk.NET.OpenGL;

namespace LearnOpenGL.Utilities;

public struct Texture
{
    private GL Gl;
    public uint ID;
    public string Type;
    public string Path;

    public Texture(GL gl)
    {
        Gl = gl;
    }
}