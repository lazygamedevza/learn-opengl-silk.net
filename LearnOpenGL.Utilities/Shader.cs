﻿using System.Numerics;
using Silk.NET.Maths;
using Silk.NET.OpenGL;

namespace LearnOpenGL.Utilities;

public class Shader : IDisposable
{
    private readonly GL GL;
    
    public uint ID { get; }

    public Shader(GL GL, string vertexShaderPath, string fragmentShaderPath)
        : this(GL, new FileInfo(vertexShaderPath), new FileInfo(fragmentShaderPath))
    {
        
    }

    public Shader(GL GL, FileInfo vertexFileInfo, FileInfo fragmentFileInfo)
    {
        this.GL = GL;
        uint vertex = this.GL.LoadShader(ShaderType.VertexShader, vertexFileInfo);
        uint fragment = GL.LoadShader(ShaderType.FragmentShader, fragmentFileInfo);

        ID = GL.CreateProgram();

        GL.AttachShader(ID, vertex);
        GL.AttachShader(ID, fragment);
        GL.LinkProgram(ID);

        GL.GetProgram(ID, ProgramPropertyARB.LinkStatus, out var status);

        if (status == 0)
        {
            throw new Exception($"Program failed to link with error: {GL.GetProgramInfoLog(ID)}");
        }

        GL.DetachShader(ID, vertex);
        GL.DetachShader(ID, fragment);
        GL.DeleteShader(vertex);
        GL.DeleteShader(fragment);
    }

    public void Use()
    {
        GL.UseProgram(ID);
    }

    // public void SetBool(string name, bool value)
    // {
    //     var location = GL.GetUniformLocation(ID, name);
    //
    //     if (location == -1)
    //     {
    //         throw new Exception($"{name} uniform not found on shader.");
    //     }
    //     
    //     GL.Uniform1(location, value);
    // }

    public void SetInt(string name, int value)
    {
        var location = GL.GetUniformLocation(ID, name);

        if (location == -1)
        {
            throw new Exception($"{name} uniform not found on shader.");
        }
        
        GL.Uniform1(location, value);
    }

    public void SetFloat(string name, float value)
    {
        var location = GL.GetUniformLocation(ID, name);

        if (location == -1)
        {
            throw new Exception($"{name} uniform not found on shader.");
        }
        
        GL.Uniform1(location, value);
    }

    public void SetVector3(string name, Vector3 vector3D)
    {
        var location = GL.GetUniformLocation(ID, name);
        
        if (location == -1)
        {
            throw new Exception($"{name} uniform not found on shader.");
        }

        GL.Uniform3(location, vector3D);
    }

    public void SetVector3(string name, float x, float y, float z)
    {
        SetVector3(name, new Vector3(x, y, z));
    }

    public unsafe void SetMatrix4(string name, in Matrix4x4 value)
    {
        var location = GL.GetUniformLocation(ID, name);

        if (location == -1)
        {
            throw new Exception($"{name} uniform not found on shader.");
        }

        fixed (Matrix4x4* valuePtr = &value)
        {
            GL.UniformMatrix4(location, 1, false, (float*)valuePtr);
        }
    }

    public void Dispose()
    {
        GL.DeleteProgram(ID);
    }
}