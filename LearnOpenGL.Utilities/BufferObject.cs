﻿using Silk.NET.OpenGL;

namespace LearnOpenGL.Utilities;

public class BufferObject<TDataType> : IDisposable where TDataType : unmanaged
{
    private uint id;
    private BufferTargetARB bufferType;
    private GL GL;

    public BufferObject(GL gl, Span<TDataType> data, BufferTargetARB bufferType)
    {
        GL = gl;
        this.bufferType = bufferType;

        id = gl.GenBuffer();
        
        GL.BufferData<TDataType>(this.bufferType, data, GLEnum.StaticDraw);
    }

    public void Bind()
    {
        GL.BindBuffer(bufferType, id);
    }

    public void Dispose()
    {
        GL.DeleteBuffer(id);
    }
}