﻿using System.Runtime.InteropServices;
using Silk.NET.OpenGL;

namespace LearnOpenGL.Utilities;

public class Mesh
{
    private readonly GL GL;
    private readonly Vertex[] vertices;
    private readonly uint[] indices;
    private readonly Texture[] textures;
    private uint VAO, VBO, EBO;

    public Mesh(GL GL, Vertex[] vertices, uint[] indices, Texture[] textures)
    {
        this.GL = GL;
        this.vertices = vertices;
        this.indices = indices;
        this.textures = textures;

        SetupMesh();
    }

    public unsafe void Draw(Shader shader)
    {
        uint diffuseNr = 1;
        uint specularNr = 1;

        for (int i = 0; i < textures.Length; i++)
        {
            GL.ActiveTexture(TextureUnit.Texture0 + i);
            // retrieve texture number (the N in diffuse_textureN)
            string number = string.Empty;
            string name = textures[i].Type;
            if (name.Equals("texture_diffuse"))
            {
                number = (diffuseNr++).ToString();
            }
            else if (name.Equals("texture_specular"))
            {
                number = (specularNr++).ToString();
            }

            shader.SetInt($"{name}{number}", i);
            GL.BindTexture(TextureTarget.Texture2D, textures[i].ID);
        }
        GL.ActiveTexture(TextureUnit.Texture0);

        GL.BindVertexArray(VAO);
        GL.DrawElements(PrimitiveType.Triangles, (uint)indices.Length, DrawElementsType.UnsignedInt, null);
    }

    private void SetupMesh()
    {
        VAO = GL.GenVertexArray();
        VBO = GL.GenBuffer();
        EBO = GL.GenBuffer();

        GL.BindVertexArray(VAO);

        GL.BindBuffer(BufferTargetARB.ArrayBuffer, VBO);
        GL.BufferData<Vertex>(BufferTargetARB.ArrayBuffer, vertices, BufferUsageARB.StaticDraw);

        GL.BindBuffer(BufferTargetARB.ElementArrayBuffer, EBO);
        GL.BufferData<uint>(BufferTargetARB.ElementArrayBuffer, indices, BufferUsageARB.StaticDraw);
        
        // vertex positions
        GL.EnableVertexAttribArray(0);
        GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, (uint)Marshal.SizeOf<Vertex>(), 0);
        // vertex normals
        GL.EnableVertexAttribArray(1);
        GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, (uint)Marshal.SizeOf<Vertex>(), Marshal.OffsetOf<Vertex>(nameof(Vertex.Normal)));
        // vertext texture coords
        GL.EnableVertexAttribArray(2);
        GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, (uint)Marshal.SizeOf<Vertex>(), Marshal.OffsetOf<Vertex>(nameof(Vertex.TexCoords)));

        GL.BindVertexArray(0);
    }
}