﻿using System.Numerics;
using Silk.NET.Assimp;
using Silk.NET.OpenGL;

namespace LearnOpenGL.Utilities;

public class Model
{
    private readonly GL GL;
    private readonly Assimp Assimp;
    private List<Mesh> Meshes;
    private List<Texture> texturesLoaded;
    
    public DirectoryInfo Directory { get; private set; }
    
    public Model(GL GL, DirectoryInfo path)
    {
        this.GL = GL;
        Assimp = Assimp.GetApi();

        Meshes = new List<Mesh>();
        texturesLoaded = new List<Texture>();
        LoadModel(path);
    }

    public void Draw(Shader shader)
    {
        foreach (var t in Meshes)
        {
            t.Draw(shader);
        }
    }

    private unsafe void LoadModel(DirectoryInfo path)
    {
        var scene = Assimp.ImportFile(path.FullName, (uint)(PostProcessSteps.Triangulate | PostProcessSteps.FlipUVs));
        
        if (scene == null || scene->MFlags == Assimp.SceneFlagsIncomplete || scene->MRootNode == null)
        {
            var error = Assimp.GetErrorStringS();
            throw new Exception(error);
        }

        Directory = path.Parent!;

        ProcessNode(scene->MRootNode, scene);
    }

    private unsafe void ProcessNode(Node* node, Scene* scene)
    {
        for (int i = 0; i < node->MNumMeshes; i++)
        {
            var mesh = scene->MMeshes[node->MMeshes[i]];
            Meshes.Add(ProcessMesh(mesh, scene));
        }

        for (int i = 0; i < node->MNumChildren; i++)
        {
            ProcessNode(node->MChildren[i], scene);
        }
    }

    private unsafe Mesh ProcessMesh(Silk.NET.Assimp.Mesh* mesh, Scene* scene)
    {
        var vertices = new Vertex[mesh->MNumVertices];
        var indices = new List<uint>();
        var textures = new List<Texture>();

        for (int i = 0; i < mesh->MNumVertices; i++)
        {
            Vertex vertex = new Vertex();

            vertex.Position = mesh->MVertices[i];

            if (mesh->MNormals != null)
            {
                vertex.Normal = mesh->MNormals[i];
            }

            if (mesh->MTangents != null)
            {
                vertex.Tangent = mesh->MTangents[i];
            }

            if (mesh->MBitangents != null)
            {
                vertex.Bitangent = mesh->MBitangents[i];
            }

            if (mesh->MTextureCoords[0] != null)
            {
                var texcoord3 = mesh->MTextureCoords[0][i];
                vertex.TexCoords = new Vector2(texcoord3.X, texcoord3.Y);
            }
            else
            {
                vertex.TexCoords = Vector2.Zero;
            }
            
            vertices[i] =(vertex);
        }

        for (int i = 0; i < mesh->MNumFaces; i++)
        {
            var face = mesh->MFaces[i];

            for (int j = 0; j < face.MNumIndices; j++)
            {
                indices.Add(face.MIndices[j]);
            }
        }

        var material = scene->MMaterials[mesh->MMaterialIndex];

        var diffuseMaps = LoadMaterialTextures(material, TextureType.Diffuse, "texture_diffuse");
        textures.AddRange(diffuseMaps);

        // var specularMaps = LoadMaterialTextures(material, TextureType.Specular, "texture_specular");
        // textures.AddRange(specularMaps);
        

        return new Mesh(GL, vertices, indices.ToArray(), textures.ToArray());
    }

    private unsafe List<Texture> LoadMaterialTextures(Material* mat, TextureType type, string typeName)
    {
        List<Texture> textures = new List<Texture>();
        
        for (uint i = 0; i < Assimp.GetMaterialTextureCount(mat, type); i++)
        {
            AssimpString str;
            Assimp.GetMaterialTexture(mat, type, i, &str, null, null, null, null, null, null);
            bool skip = false;
            for (int j = 0; j < texturesLoaded.Count; j++)
            {
                if (string.CompareOrdinal(texturesLoaded[j].Path, str.AsString) == 0)
                {
                    textures.Add(texturesLoaded[j]);
                    skip = true;
                    break;
                }
            }

            if (!skip)
            {
                var texture = new Texture(GL);
                texture.ID = GL.TextureFromFile(str.AsString, Directory);
                texture.Type = typeName;
                texture.Path = str.AsString;

                textures.Add(texture);
                texturesLoaded.Add(texture);
            }
        }

        return textures;
    }
}