﻿using Silk.NET.OpenGL;
using StbiSharp;

namespace LearnOpenGL.Utilities;

public static class OpenGLExtensions
{
    public static uint LoadShader(this GL GL, ShaderType type, FileInfo fileInfo)
    {
        string src = File.ReadAllText(fileInfo.FullName);
        uint handle = GL.CreateShader(type);
        GL.ShaderSource(handle, src);
        GL.CompileShader(handle);
        var infoLog = GL.GetShaderInfoLog(handle);

        if (!string.IsNullOrWhiteSpace(infoLog))
        {
            throw new Exception($"Error compiling shader of type {type}, failed with error {infoLog}");
        }

        return handle;
    }

    public static unsafe uint TextureFromFile(this GL GL, string path, DirectoryInfo directory, bool gamma = false)
    {
        var filename = Path.Join(directory.FullName, path);

        var textureID = GL.GenTexture();

        using var textureStream = File.OpenRead(filename);
        using var textureMemoryStream = new MemoryStream();
        textureStream.CopyTo(textureMemoryStream);
        var textureSpan = textureMemoryStream.GetBuffer().AsSpan();
        fixed (byte* texturePtr = &textureSpan.GetPinnableReference())
        {
            byte* data = Stbi.LoadFromMemory(texturePtr, textureSpan.Length,
                out var width, out var height, out var nrComponents, 0);

            if (data != null)
            {
                InternalFormat format = default;
                PixelFormat pixelFormat = default;
                if (nrComponents == 1)
                {
                    format = InternalFormat.Red;
                    pixelFormat = PixelFormat.Red;
                }
                else if (nrComponents == 3)
                {
                    format = InternalFormat.Rgb;
                    pixelFormat = PixelFormat.Rgb;
                }
                else if (nrComponents == 4)
                {
                    format = InternalFormat.Rgba;
                    pixelFormat = PixelFormat.Rgba;
                }


                GL.BindTexture(TextureTarget.Texture2D, textureID);
                GL.TexImage2D(TextureTarget.Texture2D, 0, format, (uint)width, (uint)height, 0, pixelFormat, PixelType.UnsignedByte, data);
                GL.GenerateMipmap(TextureTarget.Texture2D);
                
                GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
                GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
                GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
                GL.TexParameterI(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

                Stbi.Free(data);
            }
            else
            {
                Stbi.Free(data);

                throw new Exception($"Texture failed to load at path: {path}");
            }
        }

        return textureID;
    }
}