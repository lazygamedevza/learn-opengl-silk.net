﻿// See https://aka.ms/new-console-template for more information

using LearnOpenGL.GettingStarted;
using LearnOpenGL.Lighting;
using LearnOpenGL.ModelLoading;

switch (args.First())
{
    case "Getting Started":
    {
        using var gettingStartedApp = new GettingStartedApp();
        return gettingStartedApp.Run();
    }
    case "Lighting":
    {
        using var lightingApp = new LightingApp();
        return lightingApp.Run();
    }
    case "Model Loading":
    {
        using var modelLoadingApp = new ModelLoadingApp();
        return modelLoadingApp.Run();
    }
    default:
        throw new Exception("Run config not specified");
}

